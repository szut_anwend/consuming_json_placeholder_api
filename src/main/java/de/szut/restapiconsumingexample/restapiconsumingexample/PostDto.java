package de.szut.restapiconsumingexample.restapiconsumingexample;

import lombok.Data;

@Data
public class PostDto {
    private int userId;
    private int id;
    private String title;
    private String body;

    @Override
    public String toString() {
        return "\nPost\n" +
                "userId: " + userId +
                "\nid: " + id +
                "\ntitle: " + title +
                "\nbody: " + body;
    }
}
