package de.szut.restapiconsumingexample.restapiconsumingexample;

import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {
    private final RestTemplate restTemplate;
    private String url = "https://jsonplaceholder.typicode.com/posts";

    public RestService() {
        this.restTemplate = new RestTemplate();
    }

    public String getPostsAsJSON(){
        return this.restTemplate.getForObject(url, String.class);
    }

    public PostDto[] getPostsAsObject(){
        return this.restTemplate.getForObject(url, PostDto[].class);
    }

    public PostDto getPostById(int id){
        return this.restTemplate.getForObject(this.url+"/{id}", PostDto.class, id);
    }

    public PostDto getPostByIdWithResponseHandling(int id) {
        ResponseEntity<PostDto> response = this.restTemplate.getForEntity(this.url+"/{id}",
                PostDto.class, id);
        System.out.println(("Fetched Time: " + response.getHeaders().getDate()));
        System.out.println("Http-Statuscode: "+response.getStatusCode());
        System.out.println("Header: \n"+ response.getHeaders());
        if(response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return null;
        }
    }
}
