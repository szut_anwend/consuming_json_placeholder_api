package de.szut.restapiconsumingexample.restapiconsumingexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestapiconsumingexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestapiconsumingexampleApplication.class, args);
		RestService service = new RestService();

		String jsonResult = service.getPostsAsJSON();
		System.out.println(jsonResult);

		PostDto[] result = service.getPostsAsObject();
		for (PostDto postDto : result
			 ) {
			System.out.println(postDto);
		}

		System.out.println("Post mit der Id 10: ");
		PostDto postDto = service.getPostById(10);
		System.out.println(postDto);


		System.out.println("\nPost Nr. 1 mit der Id 1 und Responsehandling: ");
		postDto = service.getPostByIdWithResponseHandling(1);
		System.out.println(postDto);
	}
}
